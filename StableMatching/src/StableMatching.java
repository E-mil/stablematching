import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

public class StableMatching {

	private Entity[] entities = null;
	private String[] names = null;

	public StableMatching() {
		Scanner s = new Scanner(System.in);
		System.out.println("Please provide a path to an input file:\n");
		File f = new File(s.nextLine());
		s.close();
		executeProgram(f);
	}

	public StableMatching(File f) {
		executeProgram(f);
	}
	
	private void executeProgram(File f) {
		readFile(f);
		long temp = System.nanoTime();
		System.out.println(temp);
		doMatching();
		long temp2 = System.nanoTime();
		System.out.println(temp2);
		System.out.println(temp2 - temp);
		System.out.println(createOutputString());
	}

	private boolean doMatching() {
		Iterator<Integer> proposors = entities[0].getPreferences();
		Entity proposor;
		int temp;
		while (proposors.hasNext()) {
			int withoutPartner = proposors.next();
			while (withoutPartner != -1) {
				proposor = entities[withoutPartner - 1];
				temp = proposor.popPreference();
				if (temp == -1) {
					return false;
				}
				withoutPartner = entities[temp-1].propose(withoutPartner);
			}
		}
		return true;
	}
	
	private String createOutputString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < entities.length; i += 2) {
			sb.append(names[i]);
			sb.append(" -- ");
			sb.append(names[entities[i].getMatch()-1]);
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}

	private void readFile(File f) {
		try {
			Scanner sc = new Scanner(f);
			int nbrEntities = 0;
			while (sc.hasNextLine()) {
				String s = sc.nextLine();
				if (s.charAt(0) == 'n') {
					nbrEntities = Integer.parseInt(s.substring(2)) * 2;
					entities = new Entity[nbrEntities];
					names = new String[nbrEntities];
					break;
				}
			}
			for (int i = 0; i < nbrEntities; i++) {
				String s = "";
				try {
					s = sc.nextLine();
				} catch (NullPointerException e) {
					System.out.println("Wrong file format (\"n=x\" missing)");
					System.exit(0);
				}
				names[i] = s.substring(s.indexOf(" ")).trim();
				entities[i] = new Entity(i + 1);
			}
			sc.nextLine(); // Swallow empty line
			for (Entity e : entities) {
				String temp = sc.nextLine();
				e.setPreferences(temp.substring(temp.indexOf(" ")).trim());
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("Invalid filepath: " + f.getAbsolutePath() + "\n");
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		if (args.length > 0) {
			new StableMatching(new File(args[0]));
		} else {
			new StableMatching();
		}
	}
}
