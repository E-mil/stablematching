import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedList;

public class Entity {

	private int number;
	private int match;
	private LinkedList<Integer> preferences;

	public Entity(int number) {
		this.number = number;
		this.preferences = new LinkedList<Integer>();
		match = -1;
	}

	public void setPreferences(String prefString) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < prefString.length(); i++) {
			char c = prefString.charAt(i);
			if (c != ' ') {
				sb.append(c);
			} else {
				preferences.addLast(((Integer.parseInt(sb.toString()))));
				sb = new StringBuilder();
			}
		}
		preferences.push(Integer.parseInt(sb.toString()));
	}

	public Iterator<Integer> getPreferences() {
		return preferences.iterator();
	}

	public int propose(int i) {
		if (match == -1) {
			match = i;
			return -1;
		}
		for (int pref : preferences) {
			if (pref == match) {
				return i;
			} else if (pref == i) {
				int temp = match;
				match = i;
				return temp;
			}
		}
		return i; // Unnecessary
	}

	public int popPreference() {
		try {
		return preferences.pop();
		} catch (EmptyStackException e) {
			return -1;
		}
	}
	
	public int getMatch() {
		return match;
	}

	public int getNumber() {
		return number;
	}
}
